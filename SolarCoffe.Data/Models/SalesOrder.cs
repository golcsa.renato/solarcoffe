using System;
using System.Collections.Generic;

namespace SolarCoffe.Data.Models
{
    public class SalesOrder
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdateOn { get; set; }
        
        public Customer Customer { get; set; }

        public List<SalesOrderItem> SalesOrderItem { get; set; }
        
        public bool IsPaid { get; set; }
    }
}